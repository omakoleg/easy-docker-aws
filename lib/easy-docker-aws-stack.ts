import { App, Stack, StackProps } from "aws-cdk-lib";
import { Vpc } from "aws-cdk-lib/aws-ec2";
import * as iam from "aws-cdk-lib/aws-iam";
import { DockerImageAsset } from "aws-cdk-lib/aws-ecr-assets";
import { Cluster, ContainerImage } from "aws-cdk-lib/aws-ecs";
import { ApplicationLoadBalancedFargateService } from "aws-cdk-lib/aws-ecs-patterns";
import { join } from "path";

export class EasyDockerAwsStack extends Stack {
  constructor(scope: App, id: string, props?: StackProps) {
    super(scope, id, props);

    const image = new DockerImageAsset(this, "BackendImage", {
      directory: join(__dirname, "..", "service"),
    });

    // At least 2 AZ required
    const vpc = new Vpc(this, "ApplicationVpc", { maxAzs: 2 });

    const cluster = new Cluster(this, "Cluster", {
      vpc,
    });

    // Create a load-balanced Fargate service and make it public
    const service = new ApplicationLoadBalancedFargateService(
      this,
      "ApplicationFargateService",
      {
        cluster: cluster,
        cpu: 256,
        desiredCount: 1,
        taskImageOptions: {
          image: ContainerImage.fromDockerImageAsset(image),
          containerPort: 8080,
        },
        memoryLimitMiB: 512,
        publicLoadBalancer: true,
      }
    );

    // service.taskDefinition.addToTaskRolePolicy(
    //   new iam.PolicyStatement({
    //     effect: iam.Effect.ALLOW,
    //     resources: ["arn"],
    //     actions: ["ec2:SomeAction", "s3:AnotherAction"],
    //     conditions: {
    //       StringEquals: {
    //         "ec2:AuthorizedService": "codebuild.amazonaws.com",
    //       },
    //     },
    //   })
    // );
  }
}
