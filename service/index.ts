import express, { Request, Response } from "express";
import logger from "morgan";

const app = express();
const port = 8080;

app.use(logger("dev"));
app.get("/", (req: Request, res: Response) => res.send("Sample Application"));

app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
