#!/usr/bin/env node
import { App } from "aws-cdk-lib";
import { EasyDockerAwsStack } from "../lib/easy-docker-aws-stack";

const app = new App();
const stackProps = {
  env: {
    region: 'us-east-1',
    account: process.env.ACCOUNT_ID,
  },
};

new EasyDockerAwsStack(app, "easy-docker-aws", stackProps);
